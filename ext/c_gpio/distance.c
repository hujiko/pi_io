#ifndef __RUBY_H
  #define __RUBY_H
  #include <ruby.h>
#endif

#include "mount.h"
#include "base.h"
#include "internal.h"
#include "time.h"

VALUE GPIO_DISTANCE_init(VALUE self, VALUE triggerPin, VALUE echoPin) {
  struct GPIO *ptr;
  int trigger = NUM2INT(triggerPin);
  int echo    = NUM2INT(echoPin);

  GPIO_INTERNAL_validate_pin(trigger);
  GPIO_INTERNAL_validate_pin(echo);

  pi_io_mountPin(trigger);
  pi_io_mountPin(echo);

  Data_Get_Struct(self, struct GPIO, ptr);

  ptr->primaryPort   = echo;
  ptr->secondaryPort = trigger;

  GPIO_INTERNAL_set_direction(ptr->primaryPort, GPIO_OUT);
  GPIO_INTERNAL_set_direction(ptr->secondaryPort, GPIO_IN);

  return self;
}

VALUE GPIO_DISTANCE_get_distance(VALUE self) {
  struct GPIO *ptr;
  unsigned long start_time = 0, end_time, elapsed;
  char content[2];

  Data_Get_Struct(self, struct GPIO, ptr);

  GPIO_INTERNAL_set_value(ptr->secondaryPort,GPIO_HIGH);
  usleep(10);
  GPIO_INTERNAL_set_value(ptr->secondaryPort, GPIO_LOW);

  while(1) {
    GPIO_INTERNAL_get_value(ptr->primaryPort, (char *) &content);
    if(strcmp(GPIO_HIGH, content) == 0 && start_time == 0) {
      start_time = clock();
      fprintf(stderr, "Startzeit gesetzt %lu\n", start_time);
    }

    if(strcmp(GPIO_LOW, content) == 0 && start_time != 0) {
      end_time = clock();
      fprintf(stderr, "Endzeit gesetzt %lu\n", end_time);
      break;
    }
    fprintf(stderr, "Bin drin %lu\n", clock());
  }

  return INT2NUM(((end_time - start_time) / 2) / CLOCKS_PER_SEC * 1000);
}
